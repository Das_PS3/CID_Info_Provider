﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Text.RegularExpressions;

namespace CID_Info_Provider
{
    internal class Get_PKG_URL_Info
    {
        private const string CID_REGEX = @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-[A-Z\d]{16}";
        private const ushort HEADER_SIZE = 1024;
        private const string U_A = "I'll grab this... -D";
        // 1 KiB

        public PKG_Information GetInfo(string link, string cid = null, ulong size = 0)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; }); // Disable SSL certificate validation

            PKG_Information pkgInfo = new PKG_Information();

            if (Regex.IsMatch(link, @"_[1-9]\.pkg") || Regex.IsMatch(link, @"_0[1-9]\.pkg") || Regex.IsMatch(link, @"_[1-9]\d\.pkg"))
                cid = Regex.Match(link, CID_REGEX).Value;

            if (string.IsNullOrEmpty(cid)) // Get the CID straight off the PKG
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
                request.UserAgent = U_A;
                request.AddRange(0, HEADER_SIZE / 10);

                using (WebResponse response = request.GetResponse())
                using (Stream data = response.GetResponseStream())
                using (BinaryReader reader = new BinaryReader(data))
                {
                    byte[] magic = new byte[0x4];
                    byte[] cidRaw = new byte[0x24];

                    byte[] contents = reader.ReadBytes(HEADER_SIZE / 10);

                    Array.Copy(contents, 0, magic, 0, magic.Length);

                    if (BitConverter.ToString(magic) == "7F-50-4B-47") // PSP/PS3/PSV PKG
                        Array.Copy(contents, 0x30, cidRaw, 0, cidRaw.Length);
                    else if (BitConverter.ToString(magic) == "7F-43-4E-54") // PS4 PKG
                        Array.Copy(contents, 0x40, cidRaw, 0, cidRaw.Length);

                    cid = Encoding.Default.GetString(cidRaw);
                }
            }

            pkgInfo.license = 69;

            if (size == 0) // Get the license type
            {
                HttpWebRequest preLicenseChk = (HttpWebRequest)WebRequest.Create(link);
                preLicenseChk.UserAgent = U_A;
                preLicenseChk.AddRange(0, HEADER_SIZE);

                int infoOffset = 0;

                using (WebResponse response = preLicenseChk.GetResponse())
                using (Stream data = response.GetResponseStream())
                using (BinaryReader reader = new BinaryReader(data))
                {
                    byte[] magic = new byte[4];
                    byte[] rawOffset = new byte[4];
                    byte[] filesAndFoldersRaw = new byte[4];
                    byte versionCheck;

                    byte[] contents = reader.ReadBytes(HEADER_SIZE);

                    Array.Copy(contents, 0, magic, 0, magic.Length);

                    if (BitConverter.ToString(magic) == "7F-50-4B-47") // PSP/PS3/PSV PKG
                    {
                        Array.Copy(contents, 0x8, rawOffset, 0, rawOffset.Length);
                        Array.Copy(contents, 0x14, filesAndFoldersRaw, 0, filesAndFoldersRaw.Length);

                        if (BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(rawOffset);
                            Array.Reverse(filesAndFoldersRaw);
                        }

                        infoOffset = BitConverter.ToInt32(rawOffset, 0);

                        pkgInfo.FilesAndFolders = BitConverter.ToUInt32(filesAndFoldersRaw, 0);

                        versionCheck = contents[0x13];

                        byte[] ps3SystemVersionRaw = new byte[2];
                        byte[] appVersionRaw = new byte[2];
                        byte[] packageVersionRaw = new byte[2];
                        byte[] installDirectoryRaw = new byte[32];

                        pkgInfo.ps3SystemVersion = "a";
                        pkgInfo.packageVersion = "a";
                        pkgInfo.appVersion = "a";
                        pkgInfo.installDirectory = "a";

                        switch (versionCheck.ToString("X"))
                        {
                            case "A0":
                                Array.Copy(contents, 0x109, ps3SystemVersionRaw, 0, ps3SystemVersionRaw.Length);
                                Array.Copy(contents, 0x10c, packageVersionRaw, 0, packageVersionRaw.Length);
                                Array.Copy(contents, 0x10e, appVersionRaw, 0, appVersionRaw.Length);

                                break;

                            case "C0":
                                Array.Copy(contents, 0x129, ps3SystemVersionRaw, 0, ps3SystemVersionRaw.Length);
                                Array.Copy(contents, 0x12c, packageVersionRaw, 0, packageVersionRaw.Length);
                                Array.Copy(contents, 0x12e, appVersionRaw, 0, appVersionRaw.Length);

                                break;

                            case "D0":
                                Array.Copy(contents, 0x109, ps3SystemVersionRaw, 0, ps3SystemVersionRaw.Length);
                                Array.Copy(contents, 0x10c, packageVersionRaw, 0, packageVersionRaw.Length);
                                Array.Copy(contents, 0x10e, appVersionRaw, 0, appVersionRaw.Length);
                                Array.Copy(contents, 0x130, installDirectoryRaw, 0, installDirectoryRaw.Length);
                                pkgInfo.installDirectory = Encoding.UTF8.GetString(installDirectoryRaw).Replace("\0", "");

                                break;

                            case "E0":
                                Array.Copy(contents, 0x2fd, ps3SystemVersionRaw, 0, ps3SystemVersionRaw.Length);
                                Array.Copy(contents, 0x300, packageVersionRaw, 0, packageVersionRaw.Length);
                                Array.Copy(contents, 0x302, appVersionRaw, 0, appVersionRaw.Length);

                                break;

                            case "F0":
                                Array.Copy(contents, 0x129, ps3SystemVersionRaw, 0, ps3SystemVersionRaw.Length);
                                Array.Copy(contents, 0x12c, packageVersionRaw, 0, packageVersionRaw.Length);
                                Array.Copy(contents, 0x12e, appVersionRaw, 0, appVersionRaw.Length);
                                Array.Copy(contents, 0x150, installDirectoryRaw, 0, installDirectoryRaw.Length);
                                pkgInfo.installDirectory = Encoding.UTF8.GetString(installDirectoryRaw).Replace("\0", "");

                                break;

                            default: // "n/a", not available, unknown
                                pkgInfo.ps3SystemVersion = string.Empty;
                                pkgInfo.packageVersion = string.Empty;
                                pkgInfo.appVersion = string.Empty;

                                break;
                        }

                        if (!string.IsNullOrEmpty(pkgInfo.ps3SystemVersion))
                        {
                            pkgInfo.ps3SystemVersion = BitConverter.ToString(ps3SystemVersionRaw).Replace("-", ".");
                            if (pkgInfo.ps3SystemVersion == "00.00")
                                pkgInfo.ps3SystemVersion = "Undefined"; // Undefined, set to 0.00 by the developer
                        }

                        if (!string.IsNullOrEmpty(pkgInfo.packageVersion))
                        {
                            pkgInfo.packageVersion = BitConverter.ToString(packageVersionRaw).Replace("-", ".");
                            if (pkgInfo.packageVersion == "00.00")
                                pkgInfo.packageVersion = "Undefined";
                        }

                        if (!string.IsNullOrEmpty(pkgInfo.appVersion))
                        {
                            pkgInfo.appVersion = BitConverter.ToString(appVersionRaw).Replace("-", ".");
                            if (pkgInfo.appVersion == "00.00")
                                pkgInfo.appVersion = "Undefined";
                        }

                        if (pkgInfo.installDirectory == "a")
                            pkgInfo.installDirectory = "Undefined";
                    }
                }

                if (infoOffset != 0)
                {
                    HttpWebRequest licenseChk = (HttpWebRequest)WebRequest.Create(link);
                    licenseChk.UserAgent = U_A;
                    licenseChk.AddRange(infoOffset + 0xB, infoOffset + 0xC);

                    using (WebResponse response = licenseChk.GetResponse())
                    using (Stream data = response.GetResponseStream())
                    using (BinaryReader reader = new BinaryReader(data))
                        pkgInfo.license = reader.ReadByte();
                }
            }

            if (size == 0) // Get the PKG's size
            {
                HttpWebRequest sizeChk = (HttpWebRequest)WebRequest.Create(link);
                sizeChk.Method = "HEAD"; // Defaults to GET
                sizeChk.UserAgent = U_A;

                using (WebResponse response = sizeChk.GetResponse())
                    size = (ulong)response.ContentLength;
            }

            pkgInfo.link = link;
            pkgInfo.cid = cid;
            pkgInfo.size = size;

            string title = null;
            string titleid = cid.Substring(7, 9);

            if (string.IsNullOrEmpty(title)) // No title so far, check online
            {
                Check_WebStore webStore = new Check_WebStore();
                string chkStore = webStore.GetStoreLink(cid)[1];

                if (!chkStore.Contains(string.Format("{0} not found (HTTP error 404)", cid)) && !chkStore.Contains(string.Format("{0} is invalid or belongs to an unknown region (Unknown Region)", cid)))
                {
                    if (webStore.GetStoreInfo(chkStore)["title_name"] != null)
                        title = webStore.GetStoreInfo(chkStore)["title_name"].ToString();
                    else
                        title = webStore.GetStoreInfo(chkStore)["parent_name"].ToString();
                }
            }

            if (string.IsNullOrEmpty(title))
                title = ExtraTitleDatabases(cid);

            pkgInfo.title = title;

            return pkgInfo;
        }

        private string ExtraTitleDatabases(string cid)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; }); // Disable SSL certificate validation

            string titleid = cid.Substring(7, 9);

            WebClient updater = new WebClient();
            updater.Headers.Add("User-Agent", U_A);

            try
            {
                using (var stream = updater.OpenRead(string.Format("https://a0.ww.np.dl.playstation.net/tpl/np/{0}/{0}-ver.xml", titleid)))
                using (var reader = new StreamReader(stream))
                {
                    string text = reader.ReadToEnd();
                    int startTitle = text.IndexOf("<TITLE>");
                    int endTitle = text.IndexOf("</TITLE>", startTitle);
                    string title = text.Substring(startTitle + 7, endTitle - startTitle - 7);
                    return title;
                }
            }
            catch (ArgumentOutOfRangeException) { }
            catch (WebException) { }

            return cid.Substring(20);
        }

        public struct PKG_Information
        {
            public string appVersion;
            public string cid;
            public uint FilesAndFolders;
            public string installDirectory;
            public byte license;
            public string link;
            public string packageVersion;
            public string ps3SystemVersion;
            public ulong size;
            public string title;
        }
    }
}
