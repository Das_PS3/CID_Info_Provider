﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CID_Info_Provider
{
    public class About_System_Menu : Form
    {
        private const int MF_SEPARATOR = 0x800;

        private const int MF_STRING = 0x0;

        // ID for the About item on the system menu
        private const int SYSMENU_ABOUT_ID = 0x1;

        private const int SYSMENU_UPDATE_ID = 0x2;

        // P/Invoke constants
        private const int WM_SYSCOMMAND = 0x112;

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            // Get a handle to a copy of this form's system (window) menu
            IntPtr hSysMenu = GetSystemMenu(Handle, false);

            // Add a separator
            AppendMenu(hSysMenu, MF_SEPARATOR, 0, string.Empty);

            // Add the About menu item
            AppendMenu(hSysMenu, MF_STRING, SYSMENU_ABOUT_ID, "&About...");

            AppendMenu(hSysMenu, MF_STRING, SYSMENU_UPDATE_ID, "Check for &updates");
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            // Test if the About item was selected from the system menu
            if ((m.Msg == WM_SYSCOMMAND) && ((int)m.WParam == SYSMENU_ABOUT_ID))
                MessageBox.Show(string.Format("{0} by {1}\nVersion: {2}\n", Application.ProductName, Application.CompanyName, Application.ProductVersion),
                                string.Format("About {0}", Application.ProductName), MessageBoxButtons.OK, MessageBoxIcon.Information);

            if ((m.Msg == WM_SYSCOMMAND) && ((int)m.WParam == SYSMENU_UPDATE_ID) && File.Exists("Updater.exe"))
            {
                var thisAssembly = Assembly.GetExecutingAssembly().GetName();
                Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.Name, thisAssembly.Version, Assembly.GetEntryAssembly().Location));
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool AppendMenu(IntPtr hMenu, int uFlags, int uIDNewItem, string lpNewItem);

        // P/Invoke declarations
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
    }
}
