﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Security;

namespace CID_Info_Provider
{
    internal class Check_WebStore
    {
        private const string U_A = "I'll grab this... -D";
        private bool isEnglish = true;

        public bool IsEnglish { get { return isEnglish; } }

        public JObject GetStoreInfo(string link)
        {
            //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; // Disable SSL certificate validation

            WebClient updater = new WebClient();
            updater.Headers.Add("User-Agent", U_A);

            JObject json;

            using (var stream = updater.OpenRead(link))
            using (var reader = new StreamReader(stream))
                json = JObject.Parse(reader.ReadToEnd());

            return json;
        }

        public string[] GetStoreLink(string cid)
        {
            bool isLinkValid = false;

            string checkLink = string.Empty;
            string link = string.Empty;

            string[] linkCodes, checkLinkCodes;
            string[] returnLinks = { link, checkLink };

            try
            {
                switch (cid[0])
                {
                    case 'U': // US
                              // In order:
                              // US, Hong Kong, Brazil
                        linkCodes = new string[] { "en-us", "en-hk", "en-br" };
                        checkLinkCodes = new string[] { "US/en", "HK/en", "BR/en" };

                        for (int i = 0; i < linkCodes.Length; i++)
                        {
                            link = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                            checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" + checkLinkCodes[i] + "/999/" + cid;

                            isLinkValid = IsValidURL(checkLink);

                            if (isLinkValid)
                            {
                                isEnglish = true;
                                break;
                            }
                        }

                        break;

                    case 'J': // Japan
                              // In order:
                              // Hong Kong, Japan
                        linkCodes = new string[] { "en-hk", "ja-jp" };
                        checkLinkCodes = new string[] { "HK/en", "JP/ja" };

                        for (int i = 0; i < linkCodes.Length; i++)
                        {
                            link = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                            checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" + checkLinkCodes[i] + "/999/" + cid;

                            isLinkValid = IsValidURL(checkLink);

                            if (isLinkValid)
                            {
                                isEnglish = true;
                                break;
                            }
                        }

                        break;

                    case 'E': // Europe
                              // In order (English):
                              // UK, Bulgaria, Croatia, Cyprus, Czech Rebublic, Denmark, Finland, Greece, Hungary, Iceland, Ireland,
                              // Israel, Norway, Malta, Romania, Poland, Slovakia, Slovenia, Sweden, Turkey, Hong Kong
                        string[] linkCodesEnglish = { "en-gb", "en-bg", "en-hr", "en-cy", "en-cz", "en-dk", "en-fi", "en-gr", "en-hu", "en-is", "en-ie",
                                                  "en-il", "en-no", "en-mt", "en-ro", "en-pl", "en-sk", "en-si", "en-se", "en-tr", "en-hk" };
                        string[] checkLinkCodesEnglish = { "GB/en", "BG/en", "HR/en", "CY/en", "CZ/en", "DK/en", "FI/en", "GR/en", "HU/en", "IS/en", "IE/en",
                                                  "IL/en", "NO/en", "MT/en", "RO/en", "PL/en", "SK/en", "SI/en", "SE/en", "TR/en", "HK/en" };

                        // In order (Local):
                        // Spain (Spanish), France (French), Italy (Italian), Germany (German), Russia (Russian), Belgium (Dutch), Belgium (French), Denmark (Danish),
                        // Luxembourg (French), Luxembourg (German), Netherlands (Dutch), Norway (Norwegian), Austria (German), Poland (Polish), Portugal (Portuguese),
                        // Switzerland (German), Switzerland (French), Finland (Finnish), Sweden (Swedish), Switzerland (Italian), Turkey (Turkish), Ukraine (Russian)
                        string[] linkCodesLocal = { "es-es", "fr-fr", "it-it", "de-de", "ru-ru", "nl-be", "fr-be", "da-dk",
                                                "fr-lu", "de-lu", "nl-nl", "no-no", "de-at", "pl-pl", "pt-pt",
                                                "de-ch", "fr-ch", "fi-fi", "sv-se", "it-ch", "tr-tr", "ru-ua"};
                        string[] checkLinkCodesLocal = { "ES/es", "FR/fr", "IT/it", "DE/de", "RU/ru", "BE/nl", "BE/fr", "DK/da",
                                                     "LU/fr", "LU/de", "NL/nl", "NO/no", "AT/de", "PL/pl", "PT/pt",
                                                     "CH/de", "CH/fr", "FI/fi", "SE/sv", "CH/it", "TR/tr", "UA/ru"};

                        for (int i = 0; i < linkCodesEnglish.Length; i++)
                        {
                            link = "https://store.playstation.com/#!/" + linkCodesEnglish[i] + "/cid=" + cid;
                            checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" + checkLinkCodesEnglish[i] + "/999/" + cid;

                            isLinkValid = IsValidURL(checkLink);

                            if (isLinkValid)
                            {
                                isEnglish = true;
                                break;
                            }
                        }

                        if (!isLinkValid)
                            for (int i = 0; i < linkCodesLocal.Length; i++)
                            {
                                link = "https://store.playstation.com/#!/" + linkCodesLocal[i] + "/cid=" + cid;
                                checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" + checkLinkCodesLocal[i] + "/999/" + cid;

                                isLinkValid = IsValidURL(checkLink);

                                if (isLinkValid)
                                {
                                    isEnglish = false;
                                    break;
                                }
                            }

                        break;

                    case 'H': // Hong Kong
                        link = "https://store.playstation.com/#!/en-hk/cid=" + cid;
                        checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/HK/en/999/" + cid;
                        isEnglish = true;
                        isLinkValid = IsValidURL(checkLink);

                        break;

                    case 'K': // Asia
                              // In order:
                              // Hong Kong, Singapore
                        linkCodes = new string[] { "en-hk", "en-sg" };
                        checkLinkCodes = new string[] { "HK/en", "SG/en" };

                        for (int i = 0; i < linkCodes.Length; i++)
                        {
                            link = "https://store.playstation.com/#!/" + linkCodes[i] + "/cid=" + cid;
                            checkLink = "https://store.playstation.com/store/api/chihiro/00_09_000/container/" + checkLinkCodes[i] + "/999/" + cid;

                            isLinkValid = IsValidURL(checkLink);

                            if (isLinkValid)
                            {
                                isEnglish = true;
                                break;
                            }
                        }

                        break;

                    default:
                        throw new UnknownRegionException();
                }

                isLinkValid = IsValidURL(checkLink);
                if (!isLinkValid)
                    throw new NotFoundException();

                returnLinks[0] = link;
                returnLinks[1] = checkLink;

                return returnLinks;
            }
            catch (NotFoundException)
            {
                returnLinks[0] = string.Format("{0} not found (HTTP error 404)", cid);
                returnLinks[1] = string.Format("{0} not found (HTTP error 404)", cid);
                return returnLinks;
            }
            catch (UnknownRegionException)
            {
                returnLinks[0] = string.Format("{0} is invalid or belongs to an unknown region (Unknown Region)", cid);
                returnLinks[1] = string.Format("{0} is invalid or belongs to an unknown region (Unknown Region)", cid);
                return returnLinks;
            }
        }

        private static bool IsValidURL(string chkUrl)
        {
            HttpWebRequest header = (HttpWebRequest)WebRequest.Create(chkUrl);
            header.Method = "HEAD"; // Defaults to GET
            header.UserAgent = U_A;

            try
            {
                using (header.GetResponse())
                    return true;
            }
            catch (WebException)
            {
                return false;
            }
        }
    }

    [Serializable]
    internal class NotFoundException : Exception
    {
    }

    [Serializable]
    internal class UnknownRegionException : Exception
    {
    }
}
