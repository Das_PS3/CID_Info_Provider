﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CID_Info_Provider
{
    public partial class Main_Window : About_System_Menu
    {
        private const string regexCID = @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-.{16}";
        private const string regexLink = @"http.*\.pkg";
        private static bool isPkgLink = false;
        private static bool launchJSON;
        private List<string> cids = new List<string>(20);
        private INIFile configFile = new INIFile(Path.GetDirectoryName(Application.ExecutablePath) + @"\config.ini", false, true);
        private Check_WebStore store;

        public Main_Window()
        {
            store = new Check_WebStore();

            InitializeComponent();

            if (!File.Exists("config.ini"))
            {
                File.CreateText("config.ini").Dispose();

                configFile.SetValue("General", "Launch JSON", false);
            }

            launchJSON = configFile.GetValue("General", "Launch JSON", false);
            checkBox1.Checked = launchJSON;

            string entry = Clipboard.GetText();

            if (Regex.IsMatch(entry, regexLink))
            {
                Match entryLink = Regex.Match(entry, regexLink);
                linkTextBox.Text = entryLink.Value;
                retrieveBtn.Enabled = true;
                Load += new EventHandler(retrieveBtn_Click); // Send this button's click - PerformClick() does not work on load
            }
            else if (Regex.IsMatch(entry, regexCID))
            {
                Match entryCID = Regex.Match(entry, regexCID);
                cidTextBox.Text = entryCID.Value;
                checkBtn.Enabled = true;
                Load += new EventHandler(checkBtn_Click);
            }
            else
                cidTextBox.Focus();
        }

        private void browserBtn_Click(object sender, EventArgs e)
        {
            string[] storeInfo = store.GetStoreLink(cidTextBox.Text);
            string storeLink = storeInfo[0];
            string jsonContainer = storeInfo[1];

            if (!launchJSON && !string.IsNullOrEmpty(storeLink))
                Process.Start(storeLink);

            if (launchJSON && !string.IsNullOrEmpty(storeLink) && !string.IsNullOrEmpty(jsonContainer))
            {
                Process.Start(storeLink);
                Process.Start(jsonContainer);
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            launchJSON = !launchJSON;

            configFile.SetValue("General", "Launch JSON", launchJSON);
        }

        private void checkBtn_Click(object sender, EventArgs e)
        {
            string cid = cidTextBox.Text.Substring(0, 36);

            string storeLink = store.GetStoreLink(cid)[1];

            if (!storeLink.Contains(cid + " not found (HTTP error 404)") && !storeLink.Contains(cid + " is invalid or belongs to an unknown region (Unknown Region)"))
            {
                browserBtn.Enabled = true;

                JObject contentInfo = store.GetStoreInfo(storeLink);

                MatchCollection foundCids = Regex.Matches(contentInfo.ToString(), @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-[\S]{16}");

                cids.Clear();

                foreach (var item in foundCids)
                    if (item.ToString() != cid && !cids.Contains(item.ToString()))
                        cids.Add(item.ToString());

                if (cids.Count > 0)
                    cids.Sort();

                richTextBox1.SelectionStart = richTextBox1.Text.Length;

                string description = string.Empty;

                try
                {
                    description = contentInfo["long_desc"].ToString();
                }
                catch (NullReferenceException) { }

                if (!string.IsNullOrEmpty(description))
                {
                    // Clean-up
                    description = Regex.Replace(description, @"<(?i)br>", "\n");
                    description = Regex.Replace(description, @"<.*?>", "");
                    description = Regex.Replace(description, @"<img.*>", "", RegexOptions.Singleline);
                    description = description.Replace("\r\n", "\n").Replace("\n\n", "\n").Replace("\n\n\n\n", "\n\n");
                    // Clean-up
                }
                else
                    description = "No description.";

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.AppendText(contentInfo["name"] + Environment.NewLine);

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                richTextBox1.AppendText(description + Environment.NewLine + Environment.NewLine);

                if (!isPkgLink && cids.Count > 0)
                {
                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.AppendText("Associated CIDs:" + Environment.NewLine);

                    foreach (var item in cids)
                        richTextBox1.AppendText(item + Environment.NewLine);
                }

                if (!isPkgLink)
                {
                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.AppendText("\n\n");
                }

                richTextBox1.ScrollToCaret();
            }
            else
            {
                browserBtn.Enabled = false;

                richTextBox1.SelectionStart = richTextBox1.Text.Length;

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9, FontStyle.Bold);
                richTextBox1.AppendText(storeLink + "\n\n");

                richTextBox1.ScrollToCaret();
            }
        }

        private void cidTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (checkBtn.Enabled == true && (e.KeyChar == Convert.ToChar(Keys.Enter) || e.KeyChar == Convert.ToChar(Keys.Return)))
                checkBtn.PerformClick();
            else if (e.KeyChar == Convert.ToChar(Keys.Escape))
                clearBtn.PerformClick();
        }

        private void cidTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(cidTextBox.Text, regexCID) || Regex.IsMatch(cidTextBox.Text, regexCID + @"\.RAP"))
            {
                checkBtn.Enabled = true;
                browserBtn.Enabled = true;
            }
            else
            {
                checkBtn.Enabled = false;
                browserBtn.Enabled = false;
            }
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            cidTextBox.Clear();
            linkTextBox.Clear();
            richTextBox1.Clear();
            cidTextBox.Focus();
        }

        private void linkTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (retrieveBtn.Enabled == true && (e.KeyChar == Convert.ToChar(Keys.Enter) || e.KeyChar == Convert.ToChar(Keys.Return)))
                retrieveBtn.PerformClick();
            else if (e.KeyChar == Convert.ToChar(Keys.Escape))
                clearBtn.PerformClick();
        }

        private void linkTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(linkTextBox.Text, regexLink))
                retrieveBtn.Enabled = true;
            else
                retrieveBtn.Enabled = false;
        }

        private void retrieveBtn_Click(object sender, EventArgs e)
        {
            Get_PKG_URL_Info pkgInfo = new Get_PKG_URL_Info();

            var info = pkgInfo.GetInfo(linkTextBox.Text);

            cidTextBox.Text = info.cid;

            cids.Clear();

            isPkgLink = true;
            checkBtn.PerformClick();
            isPkgLink = false;

            string niceSize = string.Empty;

            ulong[] limits = new ulong[] { 1024 * 1024 * 1024, 1024 * 1024, 1024 };
            string[] units = new string[] { "GiB", "MiB", "KiB" };

            for (int i = 0; i < limits.Length; i++)
            {
                if (info.size >= limits[i])
                {
                    niceSize = string.Format("{0:#,##0.00} " + units[i], ((double)info.size / limits[i]));
                    break;
                }
            }

            if (string.IsNullOrEmpty(niceSize))
                niceSize = string.Format("{0:N0} B", info.size);

            {
                richTextBox1.SelectionStart = richTextBox1.Text.Length;

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                richTextBox1.SelectionColor = Color.Purple;
                richTextBox1.AppendText(string.Format("CID: {0}\n", info.cid));

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                richTextBox1.SelectionColor = Color.Purple;
                richTextBox1.AppendText(string.Format("Size: {0} ({1:#,#} B)\n", niceSize, info.size));

                if (info.license != 69)
                {
                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.SelectionColor = Color.Purple;
                    richTextBox1.AppendText(string.Format("Files and folders: {0}\n", info.FilesAndFolders));

                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.SelectionColor = Color.Purple;

                    string licType = string.Empty;

                    switch (info.license)
                    {
                        case 1:
                            licType = "Online";
                            break;

                        case 2:
                            licType = "Local";
                            break;

                        case 3:
                            licType = "Free";
                            break;

                        default:
                            licType = "Unknown";
                            break;
                    }

                    richTextBox1.AppendText(string.Format("License Type: {0} ({1})\n", info.license, licType));

                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.SelectionColor = Color.Purple;
                    richTextBox1.AppendText(string.Format("PS3_SYSTEM_VER: {0}\n", !string.IsNullOrEmpty(info.ps3SystemVersion) ? info.ps3SystemVersion : "N/A"));

                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.SelectionColor = Color.Purple;
                    richTextBox1.AppendText(string.Format("APP_VER: {0}\n", !string.IsNullOrEmpty(info.appVersion) ? info.appVersion : "N/A"));

                    richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                    richTextBox1.SelectionColor = Color.Purple;
                    richTextBox1.AppendText(string.Format("VERSION: {0}\n", !string.IsNullOrEmpty(info.packageVersion) ? info.packageVersion : "N/A"));

                    if (info.installDirectory != "Undefined")
                    {
                        richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                        richTextBox1.SelectionColor = Color.Red;
                        richTextBox1.AppendText(string.Format("InstallDirectory: {0}\n", info.installDirectory));
                    }

                    if (cids.Count > 0)
                    {
                        if (info.installDirectory != "Undefined")
                            richTextBox1.AppendText("\n");

                        richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                        richTextBox1.SelectionColor = Color.Black;

                        richTextBox1.AppendText("\nAssociated CIDs:\n");

                        foreach (var item in cids)
                            richTextBox1.AppendText(item + "\n");
                    }
                }

                richTextBox1.SelectionFont = new Font("Arial Unicode MS", 9);
                richTextBox1.AppendText("\n\n");

                richTextBox1.ScrollToCaret();
            }
        }
    }
}
